#!/bin/bash

BLACK='\e[30m'
RED='\e[31m'
GREEN='\e[32m'
YELLOW='\e[33m'
BLUE='\e[34m'
PURPLE='\e[35m'
CYAN='\e[36m'
WHITE='\e[37m'
NC='\e[0m'

if [ `lsb_release -c | awk '{print $2}'` != "bullseye" ]; then
	echo -e "\n${RED}[*] Your distro is not supported\n${NC}"
	exit 1
fi

echo -e "${CYAN}               _                 ___             _              "
echo "              | |__   __ ___  __/ _ \ _ __ _ __ (_)             "
echo "              | '_ \ / _' \ \/ / | | | '__| '_ \| |             "
echo "              | | | | (_| |>  <| |_| | |  | |_) | |             "
echo '              |_| |_|\__,_/_/\_\\___/|_|  | .__/|_|             '
echo "                                          |_|                   "
echo -e "${NC}"
echo -e "${RED}                  === hax0rpi Release 1.3 ===                   ${NC}"
echo -e "${RED}                   codename: Maromota Dorada                   ${NC}"
echo -e "${YELLOW}           A Raspberry Pi Hacker Tools suite by Vay3t           ${NC}"
echo ""
echo "----------------------------------------------------------------"
echo -e "${GREEN}    This installer will load a comprehensive of hacker tools    "
echo "      suite onto your Raspberry Pi. Note that the Raspbian      "
echo "     distribution must be installed onto the SD card before     "
echo -e "     proceeding. See README (if exist) for more information.    ${NC}"
echo ""
echo -e "${CYAN}[>] Press ENTER to continue, CTRL+C to abort.${NC}"
read INPUT
echo ""

# change password
#passwd pi

echo -e "${YELLOW}[!] enable ssh${NC}"
sudo systemctl enable ssh

# secret directory
secret=arsenal
cd
mkdir $secret && cd $secret
echo -e "\n${YELLOW}[+] folder 'secret' created${NC}"

# update system
echo -e "\n${YELLOW}[!] update and upgrade system${NC}"
sudo apt-get update
sudo apt-get dist-upgrade -y

# install from apt-get
echo -e "\n${YELLOW}\n[!] install tools with apt-get${NC}"
sudo apt-get install -yq \
	arp-scan \
	binwalk \
	curl \
	dhex \
	dos2unix \
	git \
	hexyl \
	hping3 \
	htop \
	hydra \
	iw \
	jq \
	libimage-exiftool-perl \
	locate \
	macchanger \
	mariadb-client \
	mariadb-server \
	nbtscan \
	netcat \
	netdiscover \
	nmap \
	openvpn \
	prips \
	proxychains4 \
	python3-dev \
	python3-pip \
	ruby-full \
	screen \
	smbclient \
	tcpdump \
	tor \
	torsocks \
	traceroute \
	tshark \
	tree \
	trickle \
	unrar \
	vim \
	wipe \
	wireless-tools \
	whois;


# install from pip
echo -e "\n${YELLOW}[!] install from pip${NC}"
sudo pip3 install --use-deprecated=legacy-resolver \
	beautifulsoup4 \
	black \
	colorama \
	dnspython \
	exrex \
	fastapi \
	flask \
	glances \
	pefile \
	progress \
	pipreqs \
	pyserv \
	python-minifier \
	python-telegram-bot \
	python-whois \
	requests \
	requests-ip-rotator \
	scapy \
	shadowsocks \
	shodan \
	slowloris \
	sqlmap \
	ssh-mitm \
	sshuttle \
	xortool \
	wafw00f;

# download github
# clone repos
echo -e "\n${YELLOW}[!] clone repos${NC}"
git clone https://github.com/maurosoria/dirsearch
git clone https://github.com/lgandx/Responder
git clone https://github.com/PowerShellMafia/PowerSploit
git clone https://github.com/samratashok/nishang
git clone https://github.com/nnposter/nndefaccts
git clone https://github.com/magnumripper/JohnTheRipper john
git clone https://github.com/OsandaMalith/IPObfuscator
git clone https://github.com/curl/h2c
git clone https://github.com/trustedsec/unicorn

# repos
git clone https://github.com/m4ll0k/BBTz
git clone https://github.com/averagesecurityguy/scripts
git clone https://github.com/random-robbie/My-Shodan-Scripts
git clone https://github.com/davidbombal/scapy

# tools
git clone https://github.com/0xacb/recollapse
git clone https://github.com/grongor/knock
git clone https://github.com/ShawnDEvans/xorpy

# install golang
cd && mkdir utilidades; cd utilidades
git clone https://github.com/udhos/update-golang
cd update-golang
sudo ./update-golang.sh
source /etc/profile.d/golang_path.sh
cd && cd $secret
curl -s https://gitlab.com/vay3t/hax0rpi/-/raw/master/golang-tools.sh | bash
curl -Ls https://github.com/ipinfo/cli/releases/download/ipinfo-2.10.0/deb.sh | sudo sh

# node
curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash -
sudo apt-get install -y nodejs
#sudo npm install -g curlconverter

# aircrack-ng
sudo apt-get install -y build-essential autoconf automake libtool pkg-config libnl-3-dev libnl-genl-3-dev libssl-dev ethtool shtool rfkill zlib1g-dev libpcap-dev libsqlite3-dev libpcre3-dev libhwloc-dev libcmocka-dev hostapd wpasupplicant tcpdump screen iw usbutils expect
git clone https://github.com/aircrack-ng/aircrack-ng
cd aircrack-ng
./autogen.sh
./configure
make
sudo make install
cd && cd $secret

# radamsa
sudo apt-get install gcc make git wget
git clone https://gitlab.com/akihe/radamsa.git && cd radamsa && make && sudo make install
cd && cd $secret

# IntruderPayloads
git clone https://github.com/1N3/IntruderPayloads
cd IntruderPayloads
./install.sh
cd && cd $secret

# searchsploit
sudo git clone https://github.com/offensive-security/exploitdb.git /opt/exploitdb
sudo ln -sf /opt/exploitdb/searchsploit /usr/local/bin/searchsploit

# metasploit
wget "https://apt.metasploit.com/$(curl -s https://apt.metasploit.com/ | grep 'arm64.deb' | tail -1 | cut -d '"' -f 2)"
sudo dpkg -i metasploit*.deb
rm metasploit*.deb
cd && cd $secret
sudo msfupdate

# fing
mkdir finggg
cd finggg
wget https://www.fing.com/images/uploads/general/CLI_Linux_Debian_5.5.2.zip
unzip CLI_Linux_Debian_5.5.2.zip
sudo dpkg -i fing-5.5.2-arm64.deb
cd ..
rm -rf finggg
cd && cd $secret

# impacket
git clone https://github.com/fortra/impacket
cd impacket
sudo python3 setup.py install
cd && cd $secret

# git-dumper
git clone https://github.com/arthaud/git-dumper
cd git-dumper
sudo pip3 install -r requirements.txt
cd && cd $secret

# smbmap
git clone https://github.com/ShawnDEvans/smbmap
cd smbmap
python3 -m pip install -r requirements.txt
cd && cd $secret

# crowbar
git clone https://github.com/galkan/crowbar
cd crowbar/
pip3 install -r requirements.txt
cd && cd $secret

# ntlm_theft
git clone https://github.com/Greenwolf/ntlm_theft
cd ntlm_theft
sudo pip3 install xlsxwriter
cd && cd $secret

# eaphammer
git clone https://github.com/s0lst1c3/eaphammer

# disable service
echo -e "\n${YELLOW}[!] disable services${NC}"
sudo systemctl disable bluetooth
sudo systemctl disable mariadb
sudo systemctl disable postgresql
sudo systemctl disable tor

# update updatedb
echo -e "\n${YELLOW}[!] updatedb${NC}"
sudo updatedb

# need reboot
echo -e "\n${GREEN}[>] Need reboot${NC}"
echo "Please run 'bash post-snap-install.sh' after reboot"

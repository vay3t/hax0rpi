#!/bin/bash

#source ~/.bashrc

#function add_env(){
#	echo 'GOBIN=$HOME/go/bin' >> ~/.bashrc
#	echo 'export PATH=$PATH:$GOBIN' >> ~/.bashrc
#}

#grep "GOBIN" ~/.bashrc &> /dev/null && true || add_env

which go &> /dev/null
if [ $? -ne 0 ]; then
	echo "[-] Please install Golang"
	exit
fi

function exit_trap() {
	local lc="$BASH_COMMAND" rc=$?
	echo "Command [$lc] exited with code [$rc]"
}

trap exit_trap err

# install golang programs

go install github.com/ericchiang/pup@latest &>/dev/null
go install github.com/maruel/panicparse/v2/cmd/pp@latest &>/dev/null
go install github.com/dnscrypt/dnscrypt-proxy/dnscrypt-proxy@latest &>/dev/null

go install github.com/projectdiscovery/httpx/cmd/httpx@latest &>/dev/null
go install github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest &>/dev/null
go install github.com/projectdiscovery/interactsh/cmd/interactsh-client@latest &>/dev/null
go install github.com/projectdiscovery/simplehttpserver/cmd/simplehttpserver@latest &>/dev/null
go install github.com/projectdiscovery/nuclei/v2/cmd/nuclei@latest &>/dev/null
go install github.com/projectdiscovery/dnsx/cmd/dnsx@latest &>/dev/null
go install github.com/projectdiscovery/katana/cmd/katana@latest &>/dev/null
go install github.com/projectdiscovery/asnmap/cmd/asnmap@latest &>/dev/null

go install github.com/ffuf/ffuf@latest &>/dev/null
go install github.com/ffuf/pencode/cmd/pencode@latest &>/dev/null

go install github.com/hahwul/gee@latest &>/dev/null
go install github.com/hahwul/dalfox/v2@latest &>/dev/null

go install github.com/tomnomnom/gron@latest &>/dev/null
go install github.com/tomnomnom/unfurl@latest &>/dev/null

go install github.com/glitchedgitz/cook/v2/cmd/cook@latest &>/dev/null
go install mvdan.cc/garble@latest &>/dev/null
go install github.com/OJ/gobuster/v3@latest &>/dev/null
go install github.com/ipinfo/cli/ipinfo@latest &>/dev/null
go install github.com/owasp-amass/amass/v3/cmd/amass@latest &>/dev/null
go install github.com/davecheney/httpstat@latest &>/dev/null
go install gitlab.com/vay3t/mubeng/cmd/mubeng@latest &>/dev/null
go install github.com/BBVA/kapow@latest &>/dev/null
go install github.com/rverton/webanalyze/cmd/webanalyze@latest &>/dev/null
go install github.com/jpillora/chisel@latest &>/dev/null
go install github.com/goretk/redress@latest &>/dev/null
go install github.com/beefsack/webify@latest &>/dev/null
go install github.com/mitchellh/gox@latest &>/dev/null

go install github.com/Binject/go-donut@latest &>/dev/null
go install github.com/Binject/backdoorfactory@latest &>/dev/null

#go install github.com/ginuerzh/gost/cmd/gost@latest &>/dev/null

go install mvdan.cc/xurls/v2/cmd/xurls@latest &>/dev/null
go install github.com/ropnop/kerbrute@latest &>/dev/null

echo "[+] Completed"
